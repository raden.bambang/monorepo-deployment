const { print } = require("@monorepo-example/print");

const sum = (num1, num2) => {
  const result = num1 + num2;
  print(result)
}

module.exports = { sum }