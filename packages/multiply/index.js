const { print } = require("@monorepo-example/print")

const multiply = (num1, num2) => {
  const result = num1 * num2;
  print(result)
}

module.exports = { multiply }